/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digital_signature;

import java.io.FileInputStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

/**
 *
 * @author Dung
 */
public class Main_ReadData {
    public static void main(String[] argv) throws Exception {
//        String certificatePath = "D:\\Work_Freelancer\\KySo_File\\cert\\CT_DIENLUC_MT.cer";
//        System.out.println(getCertificate(certificatePath));
        try{
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate cert = cf.generateCertificate(new FileInputStream("D:\\Work_Freelancer\\KySo_File\\cert\\CT_DIENLUC_MT.cer"));
            System.out.println(cert);
            }catch(Exception ex){
            ex.printStackTrace();
        }   
    }//from  w  w  w  .  j  a  v a 2 s .  c  o  m

    public static final String KEY_STORE = "JKS";
    public static final String X509 = "X.509";

    public static Certificate getCertificate(String certificatePath)
            throws Exception {
        CertificateFactory certificateFactory = CertificateFactory
                .getInstance(X509);
        FileInputStream in = new FileInputStream(certificatePath);

        Certificate certificate = certificateFactory
                .generateCertificate(in);
        in.close();

        return certificate;
    }

    public static Certificate getCertificate(String keyStorePath,
            String alias, String password) throws Exception {
        KeyStore ks = getKeyStore(keyStorePath, password);
        Certificate certificate = ks.getCertificate(alias);

        return certificate;
    }

    public static KeyStore getKeyStore(String keyStorePath, String password)
            throws Exception {
        FileInputStream is = new FileInputStream(keyStorePath);
        KeyStore ks = KeyStore.getInstance(KEY_STORE);
        ks.load(is, password.toCharArray());
        is.close();
        return ks;
    }
}
