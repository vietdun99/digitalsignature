/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digital_signature;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author Le Thien Van
 */
public class Main extends javax.swing.JFrame {

    private File certFile;
    private List<File> listPDF;
    private JFileChooser fileDialog;

    /**
     * Creates new form Main
     */
    public Main() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        certFile = null;
        listPDF = new ArrayList<File>();
        fileDialog = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pListFile = new javax.swing.JPanel();
        pAction = new javax.swing.JPanel();
        btnSelectPDF = new javax.swing.JButton();
        btnSelectCert = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout pListFileLayout = new javax.swing.GroupLayout(pListFile);
        pListFile.setLayout(pListFileLayout);
        pListFileLayout.setHorizontalGroup(
            pListFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        pListFileLayout.setVerticalGroup(
            pListFileLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 344, Short.MAX_VALUE)
        );

        btnSelectPDF.setText("Chọn PDF");
        btnSelectPDF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectPDFActionPerformed(evt);
            }
        });

        btnSelectCert.setText("Chọn chữ ký số");
        btnSelectCert.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectCertActionPerformed(evt);
            }
        });

        jButton1.setText("Hoàn tất");

        javax.swing.GroupLayout pActionLayout = new javax.swing.GroupLayout(pAction);
        pAction.setLayout(pActionLayout);
        pActionLayout.setHorizontalGroup(
            pActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pActionLayout.createSequentialGroup()
                .addContainerGap(241, Short.MAX_VALUE)
                .addComponent(btnSelectPDF)
                .addGap(18, 18, 18)
                .addComponent(btnSelectCert)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addGap(33, 33, 33))
        );
        pActionLayout.setVerticalGroup(
            pActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pActionLayout.createSequentialGroup()
                .addContainerGap(43, Short.MAX_VALUE)
                .addGroup(pActionLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSelectPDF)
                    .addComponent(btnSelectCert)
                    .addComponent(jButton1))
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(pAction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pListFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pListFile, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pAction, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSelectPDFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectPDFActionPerformed
        // TODO add your handling code here:
        fileDialog.setMultiSelectionEnabled(true);
        FileFilter filter = new FileNameExtensionFilter("PDF", "pdf");
        fileDialog.setFileFilter(filter);
        int returnValue = fileDialog.showOpenDialog(pAction);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File[] selectedFile = fileDialog.getSelectedFiles();
            listPDF.addAll(Arrays.asList(selectedFile));
        }
        for(int i = 0; i <listPDF.size(); i++){
            System.out.println(listPDF.get(i).getAbsolutePath());
        }
    }//GEN-LAST:event_btnSelectPDFActionPerformed

    private void btnSelectCertActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectCertActionPerformed
        // TODO add your handling code here:
        fileDialog.setMultiSelectionEnabled(false);
        FileFilter filter = new FileNameExtensionFilter("All files", "all files");
        fileDialog.setFileFilter(null);
        int returnValue = fileDialog.showOpenDialog(pAction);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            certFile = fileDialog.getSelectedFile();
        }
        
    }//GEN-LAST:event_btnSelectCertActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new Main().setVisible(true);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSelectCert;
    private javax.swing.JButton btnSelectPDF;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel pAction;
    private javax.swing.JPanel pListFile;
    // End of variables declaration//GEN-END:variables
}
