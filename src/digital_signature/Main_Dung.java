/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digital_signature;

import java.io.File;
import java.io.FileInputStream;
import java.security.Security;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.digitalsignature.PDSignature;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cms.*;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Store;
/**
 *
 * @author Dung
 */
public class Main_Dung {
    
    private List<String> listFilePath = new ArrayList<String>();
    
    public static void main(String []args){
        Map<String,String> data = verifyKySo("D:\\Work_Freelancer\\DK01_TCDN_Mau moi.pdf");
     }
    
    
    public void convertDataIntoList(String filePathStrInput){
        String[] fileArr = filePathStrInput.split(";");
        for(int i = 0; i < fileArr.length; i++){
            listFilePath.add(fileArr[i]);
        }
    }
    
        public static boolean isExistFile(String filePath){
            File f = new File(filePath);
            if(f.exists() && !f.isDirectory()) { 
               return true;
            }
            return false;
        }
    
        public static Map<String,String> verifyKySo(String filePathStrInput){
        
        Map<String,String> resultMap = new HashMap<String, String>();
        
        try{
            //listFilePath = getParameter("listFilePath");
//            File signedFile = new File("D:\\fileKySo\\pdf_digital_signature_timestamp.pdf");
            String filePath = filePathStrInput;
                PDDocument document = null;
                if(!filePath.isEmpty()){
                    if (isExistFile(filePath)) {
                        File signedFile = new File(filePath);
                //                    if (FileUtils.isExistFile(filePath)) { //TODO: test
                //                        File signedFile = new File(filePath); //TODO: test
                        document = PDDocument.load(signedFile, "1234");
                        PDDocumentCatalog pdCatalog = document.getDocumentCatalog();
                        PDAcroForm pdAcroForm = pdCatalog.getAcroForm();
                        if (pdAcroForm.isSignaturesExist()) {
                            Security.addProvider(new BouncyCastleProvider());
                            List<PDSignature> signatureDictionaries = document.getSignatureDictionaries();
                            X509Certificate cert;
                            Collection<X509Certificate> result = new HashSet<X509Certificate>();
                            for (PDSignature signatureDictionary : signatureDictionaries) {
                                byte[] signatureContent = signatureDictionary.getContents(new FileInputStream(signedFile));
                                byte[] signedContent = signatureDictionary.getSignedContent(new FileInputStream(signedFile));
                                CMSProcessable cmsProcessableInputStream = new CMSProcessableByteArray(signedContent);
                                CMSSignedData cmsSignedData = new CMSSignedData(cmsProcessableInputStream, signatureContent);
                                // get certificates
                                Store certStore = cmsSignedData.getCertificates();
                                // get signers
                                SignerInformationStore signers = cmsSignedData.getSignerInfos();
                                Iterator<?> it = signers.getSigners().iterator();
                                while (it.hasNext()) {
                                    SignerInformation signer = (SignerInformation) it.next();
                                    // get all certificates
                                    Collection<?> certCollection = certStore.getMatches(signer.getSID());
                                    Iterator<?> certIt = certCollection.iterator();
                                    while (certIt.hasNext()) {
                                        // print details
                                        X509CertificateHolder certificateHolder = (X509CertificateHolder) certIt.next();
                                        System.out.println("Subject:      " + certificateHolder.getSubject());                    
                                        System.out.println("Issuer:       " + certificateHolder.getIssuer());
                                        System.out.println("Valid from:   " + certificateHolder.getNotBefore());
                                        System.out.println("Valid to:     " + certificateHolder.getNotAfter());  
//                                        if(searchUserLDAP(certificateHolder.getSubject().toString(), certificateHolder.getNotBefore().toString(), certificateHolder.getNotAfter().toString())){
//                                            resultMap.put(filePath, "File hợp lệ");
//                                        }else{
//                                            resultMap.put(filePath, "File không hợp lệ");
//                                        }
                                    }
                                }
                            }
                        }else{
                            resultMap.put(filePath, "File không hợp lệ");
                            document.close();
                        }
                        document.close();
                    }else{
                        resultMap.put(filePath, "File không hợp lệ");
                    }
                }
        }catch(Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }
}
